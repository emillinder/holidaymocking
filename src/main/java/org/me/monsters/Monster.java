
package org.me.monsters;

/**
 * Monster contract.
 */
public interface Monster {

    void awaken();

    boolean isAwake();

    void signatureMove();

    boolean attack();

    void makeObey() throws CantControlTheMonsterException;

    boolean isObeying();
}
