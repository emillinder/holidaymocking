
package org.me.monsters;

/**
 * Creating monsters to say Hello world!
 */
public class Frankenstein {

    private Monster myMonster;

    public Frankenstein(Monster testMonster) {
        this.myMonster = testMonster;
    }

    public Monster create() {

        // do light limb assembly, add lightning, then
        myMonster.awaken();

        return myMonster;
    }

    public boolean isAbleToControl() {
        return myMonster.isObeying();
    }

    public void tryToControlTheMonster() throws CantControlTheMonsterException {
        myMonster.makeObey();
    }
}
