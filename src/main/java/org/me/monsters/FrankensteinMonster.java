
package org.me.monsters;

/**
 * Frank's Monster. Generally uncontrollable. Afraid of fire.
 */
public class FrankensteinMonster implements Monster {

    boolean awake;

    boolean obeying;

    @Override
    public void awaken() {
        awake = false; // just keeping the public safe
    }

    @Override
    public boolean isAwake() {
        return awake;
    }

    @Override
    public void signatureMove() {
        throw new IllegalStateException("Lame signature move");
    }

    @Override
    public boolean attack() {
        return true;
    }

    @Override
    public void makeObey() throws CantControlTheMonsterException {
        // What would happen if we were using a third-party monster controller?
        // How could we test handling of third-party exceptions?
        obeying = false;
    }

    @Override
    public boolean isObeying() {
        return obeying;
    }
}
