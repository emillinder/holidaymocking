
package org.me.monsters;

/**
 * What happens when real monsters are pushed around.
 */
public class CantControlTheMonsterException extends Exception {}
