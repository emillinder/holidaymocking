
package org.me.monsters;

/**
 * TODO BrideOfFrankenstein DOCUMENTATION
 */
public class BrideOfFrankenstein implements Monster {

    @Override
    public void awaken() {}

    @Override
    public boolean isAwake() {
        return false;
    }

    @Override
    public void signatureMove() {}

    @Override
    public boolean attack() {
        return false;
    }

    @Override
    public void makeObey() throws CantControlTheMonsterException {}

    @Override
    public boolean isObeying() {
        return false;
    }
}
