package org.me.turkey;

/**
 * A broken oven.
 */
public class BrokenOven implements Oven {
    @Override
    public Turkey cook(final Turkey turkey) {
        turkey.setQuality(Quality.UGLY);

        return turkey;
    }
}
