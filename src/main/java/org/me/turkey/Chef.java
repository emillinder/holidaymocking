package org.me.turkey;

/**
 * A chef who uses an oven to cook turkey.
 */
public class Chef {

    private Oven oven;
    private Turkey turkey;
    private String name;

    /**
     * Mock the Oven. Behavior and state can be verified.
     *
     * @param name
     * @param oven
     * @param turkey
     */
    public Chef(String name, Oven oven, Turkey turkey) {
        this.name = name;
        this.oven = oven;
        this.turkey = turkey;
    }

    public String getName() {
        return name;
    }

    public Turkey getTurkey() {
        return turkey;
    }

    public boolean removeGiblets() {
        turkey.setGiblets(null);
        return true;
    }

    public Turkey cook() {
        return oven.cook(turkey);
    }
}
