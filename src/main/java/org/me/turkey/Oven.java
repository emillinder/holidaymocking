package org.me.turkey;

/**
 * Cooks Turkey or whatever.
 */
public interface Oven {

    Turkey cook(Turkey turkey);

}
