package org.me.turkey;

import java.util.concurrent.TimeUnit;

/**
 * Never gets the bird done on time.
 */
public class ReallySlowOven implements Oven {
    @Override
    public Turkey cook(Turkey turkey) {
        try {
            Thread.sleep(TimeUnit.HOURS.toMillis(6));
            turkey.setQuality(Quality.BAD);
        } catch (InterruptedException e) {
            // Interrupted and did not finish cooking the turkey
            System.err.println("Interrupted. No cooked turkey for you.");
        }

        return turkey;
    }
}
