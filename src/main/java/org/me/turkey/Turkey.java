package org.me.turkey;

/**
 * A turkey. Possibly jive.
 */
public class Turkey {

    private boolean cooked;

    private Quality quality;

    private Object giblets;

    public boolean isCooked() {
        return cooked;
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(Quality quality) {
        this.quality = quality;
        cooked = true;
    }

    public void setGiblets(Object o) {
        this.giblets = o;
    }
}
