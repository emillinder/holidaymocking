
package org.me.turkey;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.me.turkey.Quality.UGLY;
import static org.mockito.Mockito.*;

/**
 * Testing a chef cooking a turkey.
 */
@RunWith(MockitoJUnitRunner.class)
public class TurkeyCookingTest {

    @Mock
    private Oven oven;

    @Mock
    private Oven slowOven;

    @Test
    public void cooks_spy_turkey() throws Exception {
        Turkey turkey1 = new Turkey();
        Turkey spyTurkey = spy(turkey1);

        Chef chef = new Chef("Sir Cooksalot", new BrokenOven(), spyTurkey);
        chef.cook();

        verify(spyTurkey).setQuality(UGLY);
        assertThat(spyTurkey.getQuality(), is(UGLY));

        // turkey1 was never interacted with as the spy is a copy of it
        assertThat(turkey1.getQuality(), is(nullValue()));
    }


    @Test(timeout = 1000)
    public void cooks_turkey_fails_timeout() throws Exception {
        Turkey turkey1 = new Turkey();
        Turkey spyTurkey = spy(turkey1);

        Chef chef = new Chef("Martha Stewwart", new ReallySlowOven(), spyTurkey);
        chef.cook();

        verify(spyTurkey).setQuality(UGLY);
        assertThat(spyTurkey.getQuality(), is(UGLY));

    }

    /**
     * This is an example of spies, mocks, and answers. A spy turkey is sent to a mock
     * oven which answers the {@code cook} invocation by manipulating the turkey passed
     * to the mock.
     *
     * @throws Exception
     */
    @Test(timeout = 1000)
    public void cooks_turkey_within_timeout() throws Exception {
        Turkey turkey1 = new Turkey();
        Turkey spyTurkey = spy(turkey1);
        Oven oven = mock(Oven.class);

        Chef chef = new Chef("Martha Stewwart", oven, spyTurkey);
        when(oven.cook(spyTurkey)).thenAnswer(new Answer<Turkey>() {
            @Override
            public Turkey answer(InvocationOnMock invocationOnMock) throws Throwable {
                Turkey o = (Turkey) invocationOnMock.getArguments()[0];
                o.setQuality(UGLY);
                return o;
            }
        });
        chef.cook();

        verify(spyTurkey).setQuality(UGLY);
        assertThat(spyTurkey.getQuality(), is(UGLY));

    }

}
