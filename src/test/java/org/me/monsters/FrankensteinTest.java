
package org.me.monsters;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.me.monsters.CantControlTheMonsterException;
import org.me.monsters.Frankenstein;
import org.me.monsters.FrankensteinMonster;
import org.me.monsters.Monster;

/**
 * Exercises ${@link org.me.monsters.Frankenstein}.
 */
public class FrankensteinTest {

    private Frankenstein frank;

    private Monster monster;

    @Before
    public void setUpTestMonster() {
        Monster testMonster = new FrankensteinMonster();
        frank = new Frankenstein(testMonster);
        monster = frank.create();
    }

    @Test
    public void ensure_its_not_aaaaliiiive() {
        Monster freshMonster = frank.create();
        assertThat(freshMonster, is(monster));

        // Oh no! Don't actually wake the monster!
        assertThat(freshMonster.isAwake(), is(false));
    }

    @Test
    public void rrrrraaarg_a_lot() {
        try {
            frank.tryToControlTheMonster();
        } catch (CantControlTheMonsterException e) {
            fail("This monster is the 1931 version. Controllable and lame.");
        }
        assertThat(frank.isAbleToControl(), is(false));
    }
}
