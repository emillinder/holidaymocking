
package org.me.monsters;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.me.monsters.CantControlTheMonsterException;
import org.me.monsters.Frankenstein;
import org.me.monsters.Monster;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Unit test for simple Frankenstein. Mocked with Mockito.
 */
@RunWith(MockitoJUnitRunner.class)
public class FrankensteinWithMocksTest {

    /**
     * Or assign to Mockito.mock(Monster.class) in test or @Before annotated
     * method
     */
    @Mock
    Monster testMonster;

    private Frankenstein frank;

    private Monster monster;

    @Before
    public void setUpTestMonster() {
        frank = new Frankenstein(testMonster);
        monster = frank.create();
    }

    @Test
    public void mock_and_verify_collaborator() {
        verify(monster).awaken();
    }

    @Test
    public void ensure_its_aaaaliiiive() {
        when(testMonster.isAwake()).thenReturn(true);
        assertThat(monster.isAwake(), is(true));
    }

    @Test
    public void obey() {
        try {
            frank.tryToControlTheMonster();
        } catch (CantControlTheMonsterException e) {
            fail("Monster should obey in this test");
        }
        when(monster.isObeying()).thenReturn(true);
        assertThat(frank.isAbleToControl(), is(true));
    }

    @Test(expected = CantControlTheMonsterException.class)
    public void disobey() throws CantControlTheMonsterException {
        doThrow(new CantControlTheMonsterException()).when(monster).makeObey();

        frank.tryToControlTheMonster(); // Frank goes nuts!
    }
}
